"""
script for initialization models in database postgres
"""
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

db = SQLAlchemy()


class Alarm(db.Model):
    """
    class for create table student
    """
    __tablename__ = 'Alarm_clock'

    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(64), index=True, unique=False)
    time = db.Column(db.DateTime, nullable=False, default=datetime.strftime(datetime.today(), "%b %d %Y"))

    def __init__(self, time, description):
        self.time = time
        self.description = description

    @property
    def serialize(self):
        return {
            'id': self.id,
            'time': self.time,
            'description': self.description
        }
