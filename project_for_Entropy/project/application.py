"""
script for defining backend of my application
"""
from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from flask import Flask, render_template
from flask_socketio import SocketIO
from models import Alarm
from datetime import today

app = Flask(__name__)
socketio = SocketIO(app)
app.config.from_pyfile('config.py')
db = SQLAlchemy(app)


@app.route('/')
def main_page():
    return "<html><head></head><body>My project</a>.</body></html>"


@app.route('/api/v1/alarm', methods=['POST'])
def add_alarm():
    """
    function for defining URL: /api/v1/alarm
    add new value in table Alarm_clock
    """
    if not request.is_json or 'time' not in request.get_json() or 'description' not in request.get_json():
        return bad_request('Missing required data.')
    try:
        alarm = Alarm(time=request.get_json()['time'], description=request.get_json()['description'])
        db.session.add(alarm)
        db.session.commit()
        return 'Successful'
    except Exception:
        return bad_request('Alarm does not exist.')


@app.route('/api/v1/alarm')
def show_alarm():
    """
    function for defining URL: /api/v1/alarm
    show information from table Alarm_clock
    """
    try:
        alarms = Alarm.query.filter(Alarm.time.month >= today.month,
                                    Alarm.time.year >= today.year, Alarm.time.day > today.day).all()
        results = [
            {
                "time": alarm.time,
                "description": alarm.description
            } for alarm in alarms]
        return {"count": len(results), "alarms": results}
    except Exception:
        return not_found("Alarms does not exist")


@socketio.on('my event')
def handle_my_custom_event(json):
    if not json or 'id' not in request.get_json():
        return bad_request('Missing required data.')
    alarm = Alarm.query.filter_by(id=json['id']).first()
    socketio.emit('your chosen alarm will ring in:', alarm.time)


def bad_request(message):
    """
    function for error fixing with status code = 400
    """
    response = jsonify({'error': message})
    response.status_code = 400
    return response


def not_found(message):
    """
    function for error fixing with status code = 404
    """
    response = jsonify({'error': message})
    response.status_code = 404
    return response


if __name__ == '__main__':
    socketio.run(app, debug=True)
