"""
script for create tables in database postgres
"""
from application import app
from models import db

with app.app_context():
    db.create_all()
